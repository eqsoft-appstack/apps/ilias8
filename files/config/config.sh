#!/usr/bin/env bash

# executed in config container with mounted volumes pwd and ilias data volume

if [ ! -f "/tmp/data/config.json" ] ; then
    echo "create ilias config.json"
    envsubst '\
    ${APP}\
    ${APP_NAME}\
    ${DOMAIN}\
' < tpl.config.json > /tmp/data/config.json
    chmod 750 -R /tmp/data
    chown -R www-data:root /tmp/data
else
    echo "ilias config.json already exists"
fi

if [ ! -d "/tmp/data/lucene" ] ; then
    mkdir /tmp/data/lucene
fi 

if [ ! -f "/tmp/data/ilServer.ini" ] ; then
    echo "create ilias ilServer.ini"
    envsubst '\
    ${APP}\
    ${APP_NAME}\
    ${DOMAIN}\
    ${CUSTOM_LUCENE_LOG_LEVEL}\
    ${CUSTOM_LUCENE_NUM_THREADS}\
    ${CUSTOM_LUCENE_RAM_BUFFER_SIZE}\
    ${CUSTOM_LUCENE_INDEX_MAX_FILE_SIZE_MB}\
' < tpl.ilServer.ini > /tmp/data/ilServer.ini
else
    echo "ilias ilServer.ini already exists"
fi

if [ ! -d "/tmp/html/.vscode" ] ; then
    cp -r .vscode /tmp/html/
    chown -R www-data:root /tmp/html/.vscode
fi